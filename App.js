import React, {useState, useRef, useEffect} from 'react';
import { Text, View, FlatList, SafeAreaView, Button, Dimensions } from 'react-native';
import styles from './styles';
import PrevNextButtons from './components/prevNextButtons';

const DATA = [
  {
    id: '1',
    title: 'First Item',
  },
  {
    id: '2',
    title: 'Second Item',
  },
  {
    id: '3',
    title: 'Third Item',
  },
  {
    id: '4',
    title: 'Fourth Item',
  },
  {
    id: '5',
    title: 'Fifth Item',
  },
];

const initialState = {
  currentIndex: 0,
}

function slices(item){
  return (
    <View style={styles.slideContainer}>
      <Text style={styles.slideTitle} >{item.title}</Text>
    </View>
  )
}

export default function App() {

  const [state, setState] = useState(initialState);
  const {currentIndex} = state;
  const flatList = useRef(null);

  useEffect(() => {
    flatList.current.scrollToIndex({index: currentIndex, animated: true })
  }, [currentIndex])

  const onPressPrevButton  = () => {
    setState({...state, currentIndex: currentIndex - 1})
  }

  const onPressNextButton = () => {
    setState({...state, currentIndex: currentIndex + 1})
  }

  const width = Dimensions.get('screen').width;

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.safeViewContainer}>
        <FlatList
          getItemLayout={(data, index) => (
            {length: width, offset: width * index, index}
          )}
          ref = {flatList}
          data={DATA}
          renderItem={({ item }) => slices(item)}
          keyExtractor={item => item.id}
          horizontal
          pagingEnabled
          scrollEnabled = {false}
          showsHorizontalScrollIndicator={false}
        />
      </SafeAreaView>
      <PrevNextButtons
        onPressPrevButton={onPressPrevButton}
        onPressNextButton={onPressNextButton}
        currentIndex={currentIndex}
        minIndex={0}
        maxIndex={DATA.length - 1}
      />
    </View>
  );
}

