import { StyleSheet, Dimensions } from 'react-native'

export default styles = StyleSheet.create({
  container: {
		backgroundColor: '#ccc',
    flex: 1,
	},
	safeViewContainer: {
		flex: 1,
	},
	slideContainer: {
		flex: 1,
		justifyContent: 'center',
		width: Dimensions.get('screen').width,
		alignItems: 'center',
	},
	slideTitle: {
		fontSize: 30,
		color: 'blue',
	},
	buttonsContainer: {
		paddingVertical: 10,
		flexDirection: 'row',
		justifyContent: 'space-evenly'
	},
});
