import React from 'react';
import {View, Button, Text} from 'react-native';
import styles from './../styles';

const prevNextButtons = ({onPressPrevButton, onPressNextButton, currentIndex, minIndex, maxIndex}) => {
	return(
		<View style={styles.buttonsContainer}>
			<Button 
				title='Prev'
				onPress = {onPressPrevButton}
				disabled={currentIndex === minIndex}
			/>
			<Button 
				title='Next'
				onPress = {onPressNextButton}
				disabled={currentIndex === maxIndex}
			/>
		</View>
	)
}

export default prevNextButtons;